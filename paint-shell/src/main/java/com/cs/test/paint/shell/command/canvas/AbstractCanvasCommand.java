package com.cs.test.paint.shell.command.canvas;

import com.cs.test.paint.shell.command.AbstractCommand;
import com.cs.test.paint.shell.command.CreateCommand;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellMethodAvailability;

public abstract class AbstractCanvasCommand extends AbstractCommand {

    /**
     * Method to maintain availability of {"L", "R", "B"} commands
     * @return
     */
    @ShellMethodAvailability({"L", "R", "B"})
    public Availability availabilityCheck() {
        return CreateCommand.isCanvasCreated()
                ? Availability.available()
                : Availability.unavailable("Command not available. You have to create the canvas first");
    }
}
