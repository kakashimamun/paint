package com.cs.test.paint.shell.command;

import com.cs.test.paint.service.ICanvasService;
import com.cs.test.paint.shell.front.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class AbstractCommand extends AbstractController {

    @Autowired
    protected ICanvasService canvasService;

}

