package com.cs.test.paint.shell.command;

import org.springframework.shell.ExitRequest;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class ExitCommand {
    @ShellMethod(key = {"Q","q"},value = "Quit the program.",group = "Exit")
    public void quit() {
        throw new ExitRequest();
    }
}
