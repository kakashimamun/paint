package com.cs.test.paint.shell;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;

@ComponentScan(basePackages = "com.cs.test.paint")
@SpringBootApplication
public class AppMain {

    public static void main(String[] args) throws Exception {

        String[] disabledCommands = {
                "--spring.shell.command.script.enabled=false",
//                "--spring.shell.command.stacktrace.enabled=false",
                "--spring.shell.command.quit.enabled=false",
        };
        String[] fullArgs = StringUtils.concatenateStringArrays(args, disabledCommands);

        ConfigurableApplicationContext context = SpringApplication.run(AppMain.class, fullArgs);
    }
}

