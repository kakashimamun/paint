package com.cs.test.paint.shell.front;

import com.cs.test.paint.service.entity.ServiceResponseEntity;

import java.util.function.Consumer;

public interface IResponseHandler {
    /**
     * Handle {@link ServiceResponseEntity} in UI
     * @param consumer
     * @param responseEntity
     * @param successMsg
     * @param errorMsg
     * @return
     */
    String processResponse(Consumer consumer, ServiceResponseEntity responseEntity, String successMsg, String errorMsg);
}
