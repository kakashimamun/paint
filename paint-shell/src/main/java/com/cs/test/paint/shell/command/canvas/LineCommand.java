package com.cs.test.paint.shell.command.canvas;

import com.cs.test.paint.service.IShapeService;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import javax.validation.constraints.Min;

@ShellComponent
public class LineCommand extends AbstractCanvasCommand {

    @Autowired
    IShapeService shapeService;

    @ShellMethod(key = {"L","l"},value = "Create a new line from (x1,y1) to (x2,y2).",group = "Canvas Commands")
    public String addLine(
            @ShellOption(value = {"-X1","-x1"}) @Min(value = 1) int x1,
            @ShellOption(value = {"-Y1","-y1"}) @Min(value = 1) int y1,
            @ShellOption(value = {"-X2","-x2"}) @Min(value = 1) int x2,
            @ShellOption(value = {"-Y2","-y2"}) @Min(value = 1) int y2
    ){

        ServiceResponseEntity result = shapeService.addLine(x1,y1,x2,y2);
        return this.processResponse((n)->{
                    System.out.println(this.renderCanvas(canvasService.getCanvasData()));
        },
                result,
                String.format("Line added on (%d,%d) to (%d,%d)",x1,y1,x2,y2),
                "");
    }
}
