package com.cs.test.paint.shell.event;

import lombok.Getter;

/**
 * Custom application event to notify canvas creation
 */
public class CanvasCreatedEvent  {

    @Getter
    private String msg;
    public CanvasCreatedEvent(int width, int height) {
        this.msg = String.format("canvas:%sx%s",width,height);
    }
}
