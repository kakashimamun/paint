package com.cs.test.paint.shell.front;

import com.cs.test.paint.service.entity.ServiceResponseEntity;

import java.util.function.Consumer;

public class AbstractController implements IResponseHandler, IShellRender {
    @Override
    public String processResponse(Consumer consumer, ServiceResponseEntity responseEntity, String successMsg, String errorMsg){
        if(responseEntity.isSuccess()){
            try {
                consumer.accept(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return successMsg.isEmpty()? responseEntity.getMsg():successMsg;
        }else {
            return errorMsg.isEmpty()? responseEntity.getMsg():errorMsg;
        }
    }


    @Override
    public String renderCanvas(char[][] grid){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Canvas:");
        stringBuilder.append(System.lineSeparator());
        for(int i = 0;i<grid[0].length+2;i++){
            stringBuilder.append("-");
        }
        stringBuilder.append(System.lineSeparator());

        for(int i=0;i<grid.length;i++){
            stringBuilder.append('|');
            for (int j=0;j<grid[0].length;j++){
                switch (grid[i][j]){
                    case '\u0000':
                        stringBuilder.append(' ');
                        break;
                    default:
                        stringBuilder.append(grid[i][j]);
                }
            }
            stringBuilder.append('|');
            stringBuilder.append(System.lineSeparator());
        }
        for(int i = 0;i<grid[0].length+2;i++){
            stringBuilder.append("-");
        }

        stringBuilder.append(System.lineSeparator());
        return stringBuilder.toString();
    }
}

