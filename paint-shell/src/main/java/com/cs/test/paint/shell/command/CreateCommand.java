package com.cs.test.paint.shell.command;

import com.cs.test.paint.service.ICanvasService;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import com.cs.test.paint.shell.event.CanvasEventPublisher;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import javax.validation.constraints.Min;

@ShellComponent
public class CreateCommand extends AbstractCommand{

    @Getter
    private static boolean canvasCreated = false;

    @Autowired
    private CanvasEventPublisher canvasEventPublisher;

    @ShellMethod(key = {"C","c"},value = "Create a new canvas of width w and height h.",group = "Create")
    public String create(@ShellOption(value = {"-W","-w"}) @Min(value = 1) int w,
                         @ShellOption(value = {"-H","-h"}) @Min(value = 1) int h){

        ServiceResponseEntity result = canvasService.createCanvas(w, h);

        return this.processResponse((n)->{
            canvasCreated = true;
            canvasEventPublisher.canvasCreated(w,h);
            System.out.println(this.renderCanvas(canvasService.getCanvasData()));
        },result,"","");
    }
}
