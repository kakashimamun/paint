package com.cs.test.paint.shell.prompt;

import com.cs.test.paint.shell.event.CanvasCreatedEvent;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.context.event.EventListener;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

@Component
public class PaintPromptProvider implements PromptProvider {

    public static final String DEFAULT_PROMPT = "cs-paint[canvas not created]";
    private String prompt = DEFAULT_PROMPT;

    @Override
    public AttributedString getPrompt() {
        if (prompt.equalsIgnoreCase(DEFAULT_PROMPT)) {
            return new AttributedString(prompt + ":>",
                    AttributedStyle.DEFAULT.foreground(AttributedStyle.RED));
        }
        else {
            return new AttributedString(prompt+":>",
                    AttributedStyle.DEFAULT.foreground(AttributedStyle.GREEN));
        }
    }

    @EventListener()
    public void handle(CanvasCreatedEvent event) {
        this.prompt = String.format("cs-paint[%s]",event.getMsg());
    }
}