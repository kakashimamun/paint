package com.cs.test.paint.shell.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * Event publisher for {@link CanvasCreatedEvent}
 */
@Component
public class CanvasEventPublisher {

    @Autowired
    private ApplicationEventPublisher publisher;

    public void canvasCreated(int width, int height) {
        publisher.publishEvent(new CanvasCreatedEvent(width,height));
    }

}