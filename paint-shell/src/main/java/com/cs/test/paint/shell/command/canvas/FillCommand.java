package com.cs.test.paint.shell.command.canvas;

import com.cs.test.paint.service.ICanvasService;
import com.cs.test.paint.service.IFillService;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@ShellComponent
public class FillCommand extends AbstractCanvasCommand{

    @Autowired
    private IFillService fillService;

    @ShellMethod(key = {"B","b"},value = "Bucket fill the entire area connected to (x,y) with \'colour\' c", group = "Canvas Commands")
    public String fill(
            @ShellOption(value = {"-X","-x"}) @Min(value = 1) int x,
            @ShellOption(value = {"-Y","-y"}) @Min(value = 1) int y,
            @ShellOption
                    (value = {"-C","-c"}) @Pattern(regexp = "[a-zA-Z0-9]", message = "Param should be between a-z, A-Z and 0-9")
            @Size(min = 1,max = 1,message = "Param should be only one character") String c
    ){
        ServiceResponseEntity result = fillService.fill(x, y, c.charAt(0));

        return this.processResponse((n)->{
                    System.out.println(this.renderCanvas(canvasService.getCanvasData()));
                },
                result,
                String.format("Filled empty area from (%d,%d)",x,y),
                "");
    }
}
