package com.cs.test.paint.shell.front;

public interface IShellRender {
    /**
     * Render pixels in UI
     * @param grid
     * @return
     */
    String renderCanvas(char[][] grid);
}
