package com.cs.test.paint.shell.prompt;

import com.cs.test.paint.shell.event.CanvasCreatedEvent;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.shell.jline.PromptProvider;

import static org.junit.Assert.*;

public class PaintPromptProviderTest {

    @Test
    public void getPromptDefault() {

        PaintPromptProvider p = new PaintPromptProvider();

        AttributedString a = p.getPrompt();

        Assert.assertEquals(a.toString(),PaintPromptProvider.DEFAULT_PROMPT+":>");
    }

    @Test
    public void getPromptChanged() {

        PaintPromptProvider p = new PaintPromptProvider();

        p.handle(new CanvasCreatedEvent(10,10));
        AttributedString a = p.getPrompt();

        Assert.assertEquals(a.toString(),"cs-paint[canvas:10x10]:>");
    }

    @Test
    public void handle() {
    }
}