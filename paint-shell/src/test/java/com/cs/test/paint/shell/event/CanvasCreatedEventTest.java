package com.cs.test.paint.shell.event;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CanvasCreatedEventTest {

    @Test
    public void getMsg() {
        Assert
                .assertEquals(
                        new CanvasCreatedEvent(10,10).getMsg(),"canvas:10x10");
    }
}