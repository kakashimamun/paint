package com.cs.test.paint.shell.command.canvas;

import com.cs.test.paint.service.CanvasService;
import com.cs.test.paint.service.FillService;
import com.cs.test.paint.service.ShapeService;
import com.cs.test.paint.service.entity.ServiceResponseStepBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyChar;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FillCommandTest {

    @Mock
    FillService fillService;

    @Mock
    CanvasService canvasService;

    @InjectMocks
    FillCommand command;

    @Test
    public void addLineSuccess() {
        when(fillService.fill(anyInt(),anyInt(),anyChar()))
                .thenReturn(ServiceResponseStepBuilder.builder(true).success("s").error("e").build());
        char[][] c = {{}};
        when(canvasService.getCanvasData())
                .thenReturn(c);

        String ret = command.fill(10,10,"o");

        Assert.assertEquals(ret,"Filled empty area from (10,10)");
    }

    @Test
    public void addLineFail() {
        when(fillService.fill(anyInt(),anyInt(),anyChar()))
                .thenReturn(ServiceResponseStepBuilder.builder(false).success("s").error("e").build());

        String ret = command.fill(10,10,"o");

        Assert.assertEquals(ret,"e");
    }
}