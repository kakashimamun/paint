package com.cs.test.paint.shell.command.canvas;

import com.cs.test.paint.service.CanvasService;
import com.cs.test.paint.service.ShapeService;
import com.cs.test.paint.service.entity.ServiceResponseStepBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RectangleCommandTest {

    @Mock
    CanvasService canvasService;

    @Mock
    ShapeService shapeService;

    @InjectMocks
    RectangleCommand command;

    @Test
    public void addRectangleSuccess() {
        when(shapeService.addRectangle(anyInt(),anyInt(),anyInt(),anyInt()))
                .thenReturn(ServiceResponseStepBuilder.builder(true).success("s").error("e").build());

        char[][] c = {{}};
        when(canvasService.getCanvasData())
                .thenReturn(c);

        String ret = command.addRectangle(10,10,20,20);

        Assert.assertEquals(ret,"Rectangle added on (10,10) to (20,20)");
    }

    @Test
    public void addRectangleFail() {
        when(shapeService.addRectangle(anyInt(),anyInt(),anyInt(),anyInt()))
                .thenReturn(ServiceResponseStepBuilder.builder(false).success("s").error("e").build());

        String ret = command.addRectangle(10,10,20,20);

        Assert.assertEquals(ret,"e");
    }
}