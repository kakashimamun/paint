package com.cs.test.paint.shell.command;

import com.cs.test.paint.service.CanvasService;
import com.cs.test.paint.service.ICanvasService;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import com.cs.test.paint.service.entity.ServiceResponseStepBuilder;
import com.cs.test.paint.shell.event.CanvasEventPublisher;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CreateCommandTest {

    @Mock
    CanvasService canvasService;

    @Mock
    private CanvasEventPublisher canvasEventPublisher;

    @InjectMocks
    CreateCommand createCommand;

    @Test
    public void createSuccess(){
        when(canvasService.createCanvas(10,10))
                .thenReturn(ServiceResponseStepBuilder.builder(true).success("s").error("e").build());

        char[][] c = {{}};
        when(canvasService.getCanvasData())
                .thenReturn(c);


        String ret = createCommand.create(10,10);

        Assert.assertEquals(ret,"s");
        Assert.assertTrue(CreateCommand.isCanvasCreated());
    }

    @Test
    public void createFail(){
        when(canvasService.createCanvas(10,10))
                .thenReturn(ServiceResponseStepBuilder.builder(false).success("s").error("e").build());

        String ret = createCommand.create(10,10);

        Assert.assertEquals(ret,"e");
    }

    @Test
    public void processResponse() {
        String c = new CreateCommand().processResponse(t -> {
        }, ServiceResponseStepBuilder.builder(true).success("s").error("e").build(),
                "success", "error");

        Assert.assertEquals(c,"success");
        Assert.assertNotEquals(c,"s");
    }

    @Test
    public void processResponseError() {
        String c = new CreateCommand().processResponse(t -> {
        }, ServiceResponseStepBuilder.builder(false).success("s").error("e").build(),
                "success", "error");

        Assert.assertEquals(c,"error");
        Assert.assertNotEquals(c,"e");
    }

    @Test
    public void renderCanvas() {

        char[][] c = {{'x','x','o'},{'\u0000','o','o'},{'x','\u0000','o'}};

        String ret = new CreateCommand().renderCanvas(c);

        StringBuilder sb = new StringBuilder("Canvas:"+System.lineSeparator());
        sb.append("-----"+System.lineSeparator());
        sb.append("|xxo|"+System.lineSeparator());
        sb.append("| oo|"+System.lineSeparator());
        sb.append("|x o|"+System.lineSeparator());
        sb.append("-----"+System.lineSeparator());

        Assert.assertEquals(ret,sb.toString());
    }
}