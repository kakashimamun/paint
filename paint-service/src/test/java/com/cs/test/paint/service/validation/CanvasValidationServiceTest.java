package com.cs.test.paint.service.validation;

import com.cs.test.paint.model.Canvas;
import com.cs.test.paint.model.interfaces.ICanvas;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CanvasValidationServiceTest {


    ICanvas c1;
    ICanvas c2;
    @Before
    public void createCanvas(){
        c1 = new Canvas(100,100);
        c2 = new Canvas(1000,1000);
    }


    @Test
    public void isValid() throws Exception {

        Assert.assertTrue(new CanvasValidationService().isValid(c1));
        Assert.assertFalse(new CanvasValidationService().isValid(c2));

    }

}