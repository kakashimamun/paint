package com.cs.test.paint.service.validation.rules;

import com.cs.test.paint.model.VerticalLine;
import com.cs.test.paint.model.Point;
import com.cs.test.paint.model.interfaces.ILine;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LineValidationRulesTest {
    ILine l1;
    ILine l2;
    ILine l3;

    @Before
    public void createLine(){
        l1 = new VerticalLine(new Point(2,2),new Point(12,2));
        l2 = new VerticalLine(new Point(2,2),new Point(2,12));
        l3 = new VerticalLine(new Point(2,2),new Point(5,5));
    }

    @Test
    public void validationRules() throws Exception {
        Assert.assertTrue(LineValidationRules.validationRules().allMatch(p->p.test(l1)));
        Assert.assertTrue(LineValidationRules.validationRules().allMatch(p->p.test(l2)));
        Assert.assertFalse(LineValidationRules.validationRules().allMatch(p->p.test(l3)));
    }

}