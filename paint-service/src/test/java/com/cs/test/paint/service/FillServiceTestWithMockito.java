package com.cs.test.paint.service;

import com.cs.test.paint.model.Canvas;
import com.cs.test.paint.model.Point;
import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.IPoint;
import com.cs.test.paint.model.interfaces.IShapeFactory;
import com.cs.test.paint.repositories.ICanvasRepository;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import com.cs.test.paint.service.validation.IShapeValidationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FillServiceTestWithMockito {

    @Mock
    IShapeFactory shapeFactory;

    @InjectMocks
    FillService fillService;

    @Test
    public void filledPoints() {
        when(shapeFactory.newPoint(any(Integer.class),any(Integer.class)))
                .thenAnswer(invocation ->
                     new Point(invocation
                             .getArgumentAt(0,Integer.class),invocation.getArgumentAt(1,Integer.class)));
        List<IPoint> p = fillService.filledPoints(new Canvas(10,10),
                new Point(4,5));
        Assert.assertEquals(p.size(),100);
    }
}