package com.cs.test.paint.service;

import com.cs.test.paint.model.CanvasFactory;
import com.cs.test.paint.model.ShapeFactory;
import com.cs.test.paint.model.interfaces.ICanvasFactory;
import com.cs.test.paint.model.interfaces.IShapeFactory;
import com.cs.test.paint.repositories.CanvasRepository;
import com.cs.test.paint.repositories.ICanvasRepository;
import com.cs.test.paint.service.CanvasService;
import com.cs.test.paint.service.FillService;
import com.cs.test.paint.service.ICanvasService;
import com.cs.test.paint.service.IFillService;
import com.cs.test.paint.service.validation.CanvasValidationService;
import com.cs.test.paint.service.validation.ICanvasValidationService;
import com.cs.test.paint.service.validation.IShapeValidationService;
import com.cs.test.paint.service.validation.ShapeValidationService;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;

@SpringBootConfiguration
@TestConfiguration
public class TestConfig {
    @Bean
    public IShapeFactory shapeFactory() {
        return new ShapeFactory();
    }

    @Bean
    public ICanvasRepository canvasRepository(){
        return new CanvasRepository();
    }

    @Bean
    public IShapeValidationService shapeValidationService(){
        return new ShapeValidationService();
    }

    @Bean
    public IFillService fillService(){
        return new FillService();
    }

    @Bean
    public ICanvasService canvasService() {
        return new CanvasService();
    }

    @Bean
    public ICanvasValidationService canvasValidationService(){
        return new CanvasValidationService();
    }

    @Bean
    public ICanvasFactory canvasFactory(){
        return new CanvasFactory();
    }

    @Bean
    public IShapeService shapeService(){
        return new ShapeService();
    }
}
