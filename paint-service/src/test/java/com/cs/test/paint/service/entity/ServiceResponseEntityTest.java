package com.cs.test.paint.service.entity;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ServiceResponseEntityTest {
    @Test
    public void FAILED_RESPONSE() throws Exception {
        ServiceResponseEntity r = ServiceResponseEntity.FAILED_RESPONSE("Test Failed");

        Assert.assertFalse(r.success);
        Assert.assertEquals(r.getMsg(),"Operation Failed:Test Failed");

    }

    @Test
    public void SUCCESSFUL_RESPONSE() throws Exception {

        ServiceResponseEntity r = ServiceResponseEntity.SUCCESSFUL_RESPONSE("Test Completed");

        Assert.assertTrue(r.success);
        Assert.assertEquals(r.getMsg(),"Operation Completed:Test Completed");
    }
}