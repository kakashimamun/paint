package com.cs.test.paint.service.validation.rules;

import com.cs.test.paint.model.HorizontalLine;
import com.cs.test.paint.model.VerticalLine;
import com.cs.test.paint.model.Point;
import com.cs.test.paint.model.interfaces.ILine;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class HorizontalLineValidationRulesTest {

    ILine l1;
    ILine l2;

    @Before
    public void createLine(){
        l1 = new HorizontalLine(new Point(2,2),new Point(12,2));
        l2 = new HorizontalLine(new Point(2,24),new Point(12,2));
    }

    @Test
    public void validationRules() throws Exception {
        Assert.assertTrue(HorizontalLineValidationRules.validationRules().test(l1));
        Assert.assertFalse(HorizontalLineValidationRules.validationRules().test(l2));
    }

    @Test
    public void isHorizontalLine() throws Exception {
        Assert.assertTrue(HorizontalLineValidationRules.isHorizontalLine()
                .test(new Point(2,2),new Point(12,2)));
        Assert.assertFalse(HorizontalLineValidationRules.isHorizontalLine()
                .test(new Point(2,24),new Point(12,2)));
    }

}