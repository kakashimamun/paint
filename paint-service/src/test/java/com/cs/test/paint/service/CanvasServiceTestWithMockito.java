package com.cs.test.paint.service;

import com.cs.test.paint.model.Canvas;
import com.cs.test.paint.model.interfaces.ICanvasFactory;
import com.cs.test.paint.repositories.ICanvasRepository;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import com.cs.test.paint.service.validation.ICanvasValidationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CanvasServiceTestWithMockito {


    @Mock
    ICanvasRepository canvasRepository;
    @Mock
    ICanvasValidationService canvasValidationService;
    @Mock
    ICanvasFactory canvasFactory;
    @InjectMocks
    CanvasService canvasService;


    @Test
    public void createCanvasSuccessful() throws Exception {
        when(canvasFactory.createCanvas(100,50))
                .thenReturn(new Canvas(100,50));

        when(canvasValidationService.isValid(canvasFactory.createCanvas(100,50)))
                .thenReturn(true);

        when(canvasRepository.createCanvas(canvasFactory.createCanvas(100,50)))
                .thenReturn(true);

        ServiceResponseEntity response = canvasService.createCanvas(100, 50);

        Assert.assertTrue(response.isSuccess());

    }

    @Test
    public void createCanvasValidationFail() throws Exception {
        when(canvasFactory.createCanvas(100,50))
                .thenReturn(new Canvas(100,50));

        when(canvasValidationService.isValid(canvasFactory.createCanvas(100,50)))
                .thenReturn(false);

        when(canvasRepository.createCanvas(canvasFactory.createCanvas(100,50)))
                .thenReturn(true);

        ServiceResponseEntity response = canvasService.createCanvas(100, 50);

        Assert.assertFalse(response.isSuccess());

    }
    @Test
    public void createCanvasRepoFail() throws Exception {
        when(canvasFactory.createCanvas(100,50))
                .thenReturn(new Canvas(100,50));

        when(canvasValidationService.isValid(canvasFactory.createCanvas(100,50)))
                .thenReturn(true);

        when(canvasRepository.createCanvas(canvasFactory.createCanvas(100,50)))
                .thenReturn(false);

        ServiceResponseEntity response = canvasService.createCanvas(100, 50);

        Assert.assertFalse(response.isSuccess());

    }
}