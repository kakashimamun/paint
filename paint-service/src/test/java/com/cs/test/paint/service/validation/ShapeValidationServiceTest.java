package com.cs.test.paint.service.validation;

import com.cs.test.paint.model.*;
import com.cs.test.paint.model.interfaces.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ShapeValidationServiceTest {

    IPoint p1;
    IPoint p2;

    ILine vl1;
    ILine vl2;

    ILine hl1;
    ILine hl2;

    ILine l1;
    ILine l2;

    IRectangle r1;
    IRectangle r2;

    ICanvas canvas;

    IShapeValidationService service;

    @Before
    public void createShapes(){
        canvas = new Canvas(100,100);

        p1 = new Point(2,2);
        p2 = new Point(200,200);

        hl1 = new HorizontalLine(new Point(2,2),new Point(90,2));
        hl2 = new HorizontalLine(new Point(2,2),new Point(200,2));

        vl1 = new VerticalLine(new Point(2,2),new Point(2,90));
        vl2 = new VerticalLine(new Point(2,2),new Point(2,200));

        IShapeFactory factory = new ShapeFactory();

        r1 = factory.newRectangle(new Point(2,2),new Point(10,10));
        r2 = factory.newRectangle(new Point(2,2),new Point(1000,1000));

        service = new ShapeValidationService();
    }

    @Test
    public void isValidPoint() throws Exception {
        Assert.assertTrue(service.isValid(p1));
        Assert.assertTrue(service.isValid(p2));
    }

    @Test
    public void isValidLine() throws Exception {
        Assert.assertTrue(service.isValid(vl1));
        Assert.assertTrue(service.isValid(vl2));
        Assert.assertTrue(service.isValid(hl1));
        Assert.assertTrue(service.isValid(hl2));
    }

    @Test
    public void isValidRectangle() throws Exception {
        Assert.assertTrue(service.isValid(r1));
        Assert.assertTrue(service.isValid(r2));
    }

    @Test
    public void isInCanvas() throws Exception {

        Assert.assertTrue(service.isInCanvas(p1,canvas));
        Assert.assertFalse(service.isInCanvas(p2,canvas));

        Assert.assertTrue(service.isInCanvas(vl1,canvas));
        Assert.assertFalse(service.isInCanvas(vl2,canvas));

        Assert.assertTrue(service.isInCanvas(hl1,canvas));
        Assert.assertFalse(service.isInCanvas(hl2,canvas));

        Assert.assertTrue(service.isInCanvas(r1,canvas));
        Assert.assertFalse(service.isInCanvas(r2,canvas));
    }

}