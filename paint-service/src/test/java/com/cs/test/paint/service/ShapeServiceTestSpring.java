package com.cs.test.paint.service;

import com.cs.test.paint.service.entity.ServiceResponseEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfig.class)
public class ShapeServiceTestSpring {

    @Autowired
    IShapeService shapeService;

    @Autowired
    ICanvasService canvasService;

    @Test
    public void addLineSuccess() {
        canvasService.createCanvas(10,10);
        ServiceResponseEntity r = shapeService.addLine(2,2,2,8);
        Assert.assertTrue(r.isSuccess());
    }

    @Test
    public void addLineInValid() {
        canvasService.createCanvas(10,10);
        ServiceResponseEntity r = shapeService.addLine(2,2,2,12);
        Assert.assertFalse(r.isSuccess());
    }

    @Test
    public void addRectangleInValid() {
        canvasService.createCanvas(10,10);
        ServiceResponseEntity r = shapeService.addRectangle(2,7,5,12);
        Assert.assertFalse(r.isSuccess());
    }
    @Test
    public void addRectangleInvalid2() {
        canvasService.createCanvas(10,10);
        ServiceResponseEntity r = shapeService.addRectangle(2,3,2,6);
        Assert.assertFalse(r.isSuccess());
    }
    @Test
    public void addRectangleSuccess() {
        canvasService.createCanvas(10,10);
        ServiceResponseEntity r = shapeService.addRectangle(2,3,7,6);
        Assert.assertTrue(r.isSuccess());
    }
}