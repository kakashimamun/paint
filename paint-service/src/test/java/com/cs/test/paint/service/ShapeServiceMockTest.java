package com.cs.test.paint.service;

import com.cs.test.paint.model.*;
import com.cs.test.paint.model.interfaces.IShapeFactory;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import com.cs.test.paint.service.entity.ServiceResponseStepBuilder;
import com.cs.test.paint.service.validation.IShapeValidationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ShapeServiceMockTest {

    @Mock
    IShapeValidationService validationService;

    @Mock
    IShapeFactory shapeFactory;

    @Mock
    ICanvasService canvasService;

    @InjectMocks
    ShapeService shapeService;

    @Test
    public void addLineSuccess() {
        when(canvasService.getCanvas()).thenReturn(new Canvas(10,10));
        when(shapeFactory.newPoint(anyInt(),anyInt())).thenReturn(new Point(2,2));
        when(shapeFactory.newVerticalLine(any(),any()))
                .thenReturn(new VerticalLine(new Point(2,2),new Point(2,12)));
        when(shapeFactory.newHorizontalLine(any(),any()))
                .thenReturn(new HorizontalLine(new Point(2,12),new Point(2,12)));

        when(validationService.isValid(any())).thenReturn(true);
        when(validationService.isInCanvas(any(),any())).thenReturn(true);

        when(canvasService.addShape(any()))
                .thenReturn(ServiceResponseStepBuilder.builder(true).success("").error("").build());

        ServiceResponseEntity r = shapeService.addLine(2, 2, 2, 12);

        Assert.assertTrue(r.isSuccess());
    }

    @Test
    public void addLineInValid() {
        when(canvasService.getCanvas()).thenReturn(new Canvas(10,10));
        when(shapeFactory.newPoint(anyInt(),anyInt())).thenReturn(new Point(2,2));
        when(shapeFactory.newVerticalLine(any(),any())).thenReturn(new VerticalLine(new Point(2,2)
                ,new Point(2,12)));
        when(shapeFactory.newHorizontalLine(any(),any())).thenReturn(new HorizontalLine(new Point(2,12)
                ,new Point(2,12)));

        when(validationService.isValid(any())).thenReturn(false);
        when(validationService.isInCanvas(any(),any())).thenReturn(true);

        when(canvasService.addShape(any()))
                .thenReturn(ServiceResponseStepBuilder.builder(true).success("").error("").build());

        ServiceResponseEntity r = shapeService.addLine(2, 2, 2, 12);

        Assert.assertFalse(r.isSuccess());
    }

    @Test
    public void addLineNotIncanvas() {
        when(canvasService.getCanvas()).thenReturn(new Canvas(10,10));
        when(shapeFactory.newPoint(anyInt(),anyInt())).thenReturn(new Point(2,2));
        when(shapeFactory.newVerticalLine(any(),any()))
                .thenReturn(new VerticalLine(new Point(2,2),new Point(2,12)));
        when(shapeFactory.newHorizontalLine(any(),any()))
                .thenReturn(new HorizontalLine(new Point(2,12),new Point(2,12)));
        when(validationService.isValid(any())).thenReturn(true);
        when(validationService.isInCanvas(any(),any())).thenReturn(false);

        when(canvasService.addShape(any()))
                .thenReturn(ServiceResponseStepBuilder.builder(true).success("").error("").build());

        ServiceResponseEntity r = shapeService.addLine(2, 2, 2, 12);

        Assert.assertFalse(r.isSuccess());
    }

    @Test
    public void addLineNotValid() {
        when(canvasService.getCanvas()).thenReturn(new Canvas(10,10));
        when(shapeFactory.newPoint(anyInt(),anyInt())).thenReturn(new Point(2,2));
        when(shapeFactory.newVerticalLine(any(),any()))
                .thenReturn(new VerticalLine(new Point(2,2),new Point(2,12)));
        when(shapeFactory.newHorizontalLine(any(),any()))
                .thenReturn(new HorizontalLine(new Point(2,12),new Point(2,12)));

        when(validationService.isValid(any())).thenReturn(false);
        when(validationService.isInCanvas(any(),any())).thenReturn(false);

        when(canvasService.addShape(any()))
                .thenReturn(ServiceResponseStepBuilder.builder(true).success("").error("").build());

        ServiceResponseEntity r = shapeService.addLine(2, 2, 2, 12);

        Assert.assertFalse(r.isSuccess());
    }

    @Test
    public void addRectangle() {
        when(canvasService.getCanvas()).thenReturn(new Canvas(10,10));
        when(shapeFactory.newPoint(anyInt(),anyInt())).thenReturn(new Point(2,2));

        when(validationService.isValid(any())).thenReturn(true);
        when(validationService.isInCanvas(any(),any())).thenReturn(true);

        when(canvasService.addShape(any()))
                .thenReturn(ServiceResponseStepBuilder.builder(true).success("").error("").build());
        when(shapeFactory.newRectangle(any(),any())).thenReturn(new Rectangle(null,null,null,null));

        ServiceResponseEntity r = shapeService.addRectangle(2, 2, 2, 12);

        Assert.assertTrue(r.isSuccess());
    }

    @Test
    public void addRectangleInValid() {
        when(canvasService.getCanvas()).thenReturn(new Canvas(10,10));
        when(shapeFactory.newPoint(anyInt(),anyInt())).thenReturn(new Point(2,2));

        when(validationService.isValid(any())).thenReturn(false);
        when(validationService.isInCanvas(any(),any())).thenReturn(false);

        when(canvasService.addShape(any()))
                .thenReturn(ServiceResponseStepBuilder.builder(true).success("").error("").build());

        ServiceResponseEntity r = shapeService.addRectangle(2, 2, 2, 12);

        Assert.assertFalse(r.isSuccess());
    }
    @Test
    public void addRectangleInValid1() {
        when(canvasService.getCanvas()).thenReturn(new Canvas(10,10));
        when(shapeFactory.newPoint(anyInt(),anyInt())).thenReturn(new Point(2,2));

        when(validationService.isValid(any())).thenReturn(false);
        when(validationService.isInCanvas(any(),any())).thenReturn(true);

        when(canvasService.addShape(any()))
                .thenReturn(ServiceResponseStepBuilder.builder(true).success("").error("").build());

        ServiceResponseEntity r = shapeService.addRectangle(2, 2, 2, 12);

        Assert.assertFalse(r.isSuccess());
    }
}