package com.cs.test.paint.service;

import com.cs.test.paint.exception.NotInitializedException;
import com.cs.test.paint.model.*;
import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.IShape;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfig.class)
public class CanvasServiceTestWithSpring {

    @Autowired
    ICanvasService canvasService;

    @Test
    public void createCanvas() throws Exception {

        ServiceResponseEntity response = canvasService.createCanvas(100, 50);
        Assert.assertTrue(response.isSuccess());

    }

    /**
     * Will fail if ran with full file/project. cause canvas bean will be initialized before
     * But will run successfully as a single test. Hence ignored
     * @throws Exception
     */
    @Ignore
    @Test(expected = NotInitializedException.class)
    public void getCanvasNotInitialized() throws Exception {
        ICanvas canvas = canvasService.getCanvas();
    }

    @Test
    public void getCanvasInitialized() throws Exception {
        canvasService.createCanvas(100, 50);
        ICanvas canvas = canvasService.getCanvas();

        Assert.assertEquals(canvas.getWidth(),100);
        Assert.assertEquals(canvas.getHeight(),50);
    }

    @Test
    public void getCanvasData() throws Exception {
        canvasService.createCanvas(100, 50);
        char[][] grid = canvasService.getCanvasData();

        Assert.assertEquals(grid.length,50);
        Assert.assertEquals(grid[0].length,100);
    }

    @Test
    public void addShapeSuccess() throws Exception {

        IShape l = (IShape) new VerticalLine(new Point(2,12),new Point(2,12));

        canvasService.createCanvas(100, 50);
        ServiceResponseEntity response = canvasService.addShape(l);

        Assert.assertTrue(response.isSuccess());
    }
}