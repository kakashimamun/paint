package com.cs.test.paint.service.validation.rules;

import com.cs.test.paint.model.Point;
import com.cs.test.paint.model.HorizontalLine;
import com.cs.test.paint.model.VerticalLine;
import com.cs.test.paint.model.interfaces.ILine;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VerticalLineValidationRulesTest {

    ILine l1;
    ILine l2;

    @Before
    public void createLine(){
        l1 = new VerticalLine(new Point(2,2),new Point(2,12));
        l2 = new VerticalLine(new Point(2,24),new Point(12,2));
    }

    @Test
    public void validationRules() throws Exception {
        Assert.assertTrue(VerticalLineValidationRules.validationRules().test(l1));
        Assert.assertFalse(VerticalLineValidationRules.validationRules().test(l2));
    }

    @Test
    public void isVerticalLine() throws Exception {
        Assert.assertTrue(VerticalLineValidationRules.isVerticalLine()
                .test(new Point(2,2),new Point(2,12)));
        Assert.assertFalse(VerticalLineValidationRules.isVerticalLine()
                .test(new Point(2,24),new Point(12,2)));
    }
}