package com.cs.test.paint.service;

import com.cs.test.paint.model.Canvas;
import com.cs.test.paint.model.Point;
import com.cs.test.paint.model.interfaces.IPoint;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.mockito.Matchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfig.class)
public class FillServiceTestWithSpring {

    @Autowired
    IFillService fillService;

    @Autowired
    ICanvasService canvasService;

    @Test
    public void filledPoints() {
        List<IPoint> p = fillService.filledPoints(new Canvas(10,10),
                new Point(4,5));
        Assert.assertEquals(p.size(),100);
    }

    @Test
    public void fillSuccess() {
        canvasService.createCanvas(10,10);
        ServiceResponseEntity r = fillService.fill(2,2,'o');
        Assert.assertTrue(r.isSuccess());
    }
}