package com.cs.test.paint.service.entity;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ServiceResponseStepBuilderTest {
    @Test
    public void testSuccess() throws Exception {
        ServiceResponseEntity s = ServiceResponseStepBuilder.builder(true).success("success").error("error").build();

        Assert.assertTrue(s.isSuccess());
        Assert.assertEquals(s.getMsg(),"success");

    }

    @Test
    public void testError() throws Exception {
        ServiceResponseEntity s = ServiceResponseStepBuilder.builder(false).success("success").error("error").build();

        Assert.assertFalse(s.isSuccess());
        Assert.assertEquals(s.getMsg(),"error");

    }

}