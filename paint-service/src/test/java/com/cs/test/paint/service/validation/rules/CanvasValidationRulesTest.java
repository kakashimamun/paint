package com.cs.test.paint.service.validation.rules;

import com.cs.test.paint.model.Canvas;
import com.cs.test.paint.model.interfaces.ICanvas;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CanvasValidationRulesTest {

    ICanvas c1;
    ICanvas c2;

    @Before
    public void createCanvas(){
        c1 = new Canvas(100,100);
        c2 = new Canvas(1000,1000);
    }

    @Test
    public void validationRules() throws Exception {
        Assert.assertTrue(CanvasValidationRules.validationRules().allMatch(p->p.test(c1)));
        Assert.assertFalse(CanvasValidationRules.validationRules().allMatch(p->p.test(c2)));
    }

    @Test
    public void widthRule() throws Exception {

        Assert.assertTrue(CanvasValidationRules.widthRule().test(c1));
        Assert.assertFalse(CanvasValidationRules.widthRule().test(c2));
    }

    @Test
    public void heightRule() throws Exception {

        Assert.assertTrue(CanvasValidationRules.heightRule().test(c1));
        Assert.assertFalse(CanvasValidationRules.heightRule().test(c2));
    }

}