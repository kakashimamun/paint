package com.cs.test.paint.service.validation.rules;

import com.cs.test.paint.model.interfaces.ICanvas;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * {@link ICanvas} validation rules
 */
public class CanvasValidationRules {

    private static final int MAX_WIDTH = 200;
    private static final int MAX_HEIGHT = 200;

    public static Stream<Predicate<ICanvas>> validationRules() {
        return Stream.of(
                widthRule(),
                heightRule()
                );
    }

    public static Predicate<ICanvas> widthRule(){

        return c -> c.getWidth() <= MAX_WIDTH;
    }
    public static Predicate<ICanvas> heightRule(){

        return c -> c.getHeight() <= MAX_HEIGHT;
    }

    public static String explanation(){
        return String.format("Canvas max width %d, Max height d",MAX_WIDTH,MAX_HEIGHT);
    }
}
