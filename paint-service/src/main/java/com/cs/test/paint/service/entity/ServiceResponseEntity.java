package com.cs.test.paint.service.entity;

import lombok.Builder;
import lombok.Getter;

/**
 * Helper class to forward service response to controllers
 */
@Builder
@Getter
public class ServiceResponseEntity {

    boolean success;
    String msg;

    public static final ServiceResponseEntity FAILED_RESPONSE(String msg){
        return ServiceResponseEntity.builder().success(false).msg("Operation Failed:"+msg).build();
    }

    public static final ServiceResponseEntity SUCCESSFUL_RESPONSE(String msg){
        return ServiceResponseEntity.builder().success(true).msg("Operation Completed:"+msg).build();
    }


}
