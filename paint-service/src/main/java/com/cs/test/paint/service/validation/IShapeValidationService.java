package com.cs.test.paint.service.validation;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.IShape;

public interface IShapeValidationService {

    /**
     * validate a {@link IShape} instance
     * @param shape
     * @return
     */
    public boolean isValid(IShape shape);

    /**
     * checks if {@link IShape} instance is with in the given {@link ICanvas}
     * @param shape
     * @param canvas
     * @return
     */
    public boolean isInCanvas(IShape shape, ICanvas canvas);
}
