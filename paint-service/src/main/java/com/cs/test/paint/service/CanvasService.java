package com.cs.test.paint.service;

import com.cs.test.paint.exception.FailedToSaveException;
import com.cs.test.paint.exception.NotInitializedException;
import com.cs.test.paint.model.interfaces.*;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import com.cs.test.paint.service.entity.ServiceResponseStepBuilder;
import com.cs.test.paint.service.validation.ICanvasValidationService;
import com.cs.test.paint.service.validation.IShapeValidationService;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cs.test.paint.repositories.ICanvasRepository;
import com.cs.test.paint.exception.FailedToCreateException;

import java.util.Optional;

@Service
@Log4j
public class CanvasService implements ICanvasService {

    @Autowired
    ICanvasRepository canvasRepository;

    @Autowired
    ICanvasValidationService canvasValidationService;

    @Autowired
    ICanvasFactory canvasFactory;

    @Override
    public ServiceResponseEntity createCanvas(int width, int height){

        try {
            ICanvas canvas = canvasFactory.createCanvas(width,height);

            if(canvasValidationService.isValid(canvas)){
                log.info("Canvas created Successfully");
                return ServiceResponseStepBuilder.builder(canvasRepository.createCanvas(canvas))
                        .success("Canvas created Successfully")
                        .error("Error occurred while creating canvas in repository.")
                        .build();
            }else{
                log.error("Validation failed while creating canvas");
                return ServiceResponseEntity.FAILED_RESPONSE("Canvas failed validation.");
            }
        }catch (FailedToCreateException e){
            e.printStackTrace();
            log.error("FailedToCreateException while creating canvas");
            return ServiceResponseEntity.FAILED_RESPONSE(e.getMessage());
        }
    }

    @Override
    public ICanvas getCanvas(){
        try {
            return canvasRepository.getCanvas();
        }catch (NotInitializedException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public char[][] getCanvasData(){
        return Optional.ofNullable(this.getCanvas().getGrid()).orElse(new char[0][0]);
    }

    @Override
    public ServiceResponseEntity addShape(IShape shape) {
        ICanvas canvas = canvasRepository.addPoints(shape.getPoints());
        log.info("Shape added to canvas");
        try {
            boolean result = canvasRepository.saveCanvas(canvas);
            return ServiceResponseStepBuilder.builder(result)
                    .success("Added shape and Canvas saved Successfully")
                    .error("Error occurred while saving canvas.")
                    .build();
        }catch (FailedToSaveException e){
            e.printStackTrace();
            log.error("Failed to save canvas");
            return ServiceResponseEntity.FAILED_RESPONSE("FailedToSaveException in saving canvas");
        }
    }

}