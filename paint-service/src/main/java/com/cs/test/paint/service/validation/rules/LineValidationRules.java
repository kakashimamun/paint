package com.cs.test.paint.service.validation.rules;

import com.cs.test.paint.model.interfaces.ILine;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Combined validation rules for all types of {@link ILine}
 */
public class LineValidationRules {
    public static Stream<Predicate<ILine>> validationRules() {
        return Stream.of(
                lineValidationRule()
        );
    }
    private static Predicate<ILine> lineValidationRule(){
        return VerticalLineValidationRules.validationRules().or(HorizontalLineValidationRules.validationRules());
    }

    public static String explanation(){
        return VerticalLineValidationRules.explanation()+" or "+ HorizontalLineValidationRules.explanation();
    }
}
