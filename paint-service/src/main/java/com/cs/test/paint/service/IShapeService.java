package com.cs.test.paint.service;

import com.cs.test.paint.service.entity.ServiceResponseEntity;

public interface IShapeService {
    /**
     * Add supported {@link com.cs.test.paint.model.interfaces.ILine} to the canvas
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    ServiceResponseEntity addLine(int x1, int y1, int x2, int y2);

    /**
     * Add {@link com.cs.test.paint.model.interfaces.IRectangle}  to the canvas
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    ServiceResponseEntity addRectangle(int x1, int y1, int x2, int y2);
}
