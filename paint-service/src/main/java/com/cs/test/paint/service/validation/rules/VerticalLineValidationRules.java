package com.cs.test.paint.service.validation.rules;

import com.cs.test.paint.model.interfaces.ILine;
import com.cs.test.paint.model.interfaces.IPoint;

import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * validation rules for {@link com.cs.test.paint.model.VerticalLine}
 */
public class VerticalLineValidationRules {

    public static Predicate<ILine> validationRules(){
        return l->l.getA().getX()==l.getB().getX();
    }

    public static BiPredicate<IPoint,IPoint> isVerticalLine(){
        return (a,b)->a.getX()==b.getX();
    }

    public static String explanation(){
        return String.format("For vertical lines between (x1,y1) & (x2,y2): x1 ==x2");
    }
}
