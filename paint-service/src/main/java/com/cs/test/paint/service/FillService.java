package com.cs.test.paint.service;

import com.cs.test.paint.exception.FailedToSaveException;
import com.cs.test.paint.exception.NotInitializedException;
import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.IPoint;
import com.cs.test.paint.model.interfaces.IShapeFactory;
import com.cs.test.paint.repositories.ICanvasRepository;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import com.cs.test.paint.service.entity.ServiceResponseStepBuilder;
import com.cs.test.paint.service.validation.IShapeValidationService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

@Service
@Log4j
public class FillService implements IFillService {

    @Autowired
    IShapeFactory shapeFactory;

    @Autowired
    ICanvasRepository canvasRepository;

    @Autowired
    IShapeValidationService shapeValidationService;

    @Override
    public List<IPoint> filledPoints(ICanvas canvas, IPoint point){
        List<IPoint> filledPoints = new ArrayList<>();
        Stack<IPoint> pointStack = new Stack<>();
        boolean[][] isVisited = makeIsVisited(canvas);

        pointStack.add(point);
        isVisited[point.getY()-1][point.getX()-1] = true;
        filledPoints.add(point);

        while (!pointStack.isEmpty()){
            IPoint p = pointStack.pop();
            List<IPoint> children = getChildren(p);

            for(IPoint cp:children){
                if(isValidPoint(canvas,cp,isVisited)){
                    pointStack.push(cp);
                    isVisited[cp.getY()-1][cp.getX()-1] = true;
                    filledPoints.add(cp);
                }
            }
        }

        if (filledPoints.isEmpty()) {
            log.error("Couldn't fill any point.");
        } else {
            log.info("Points filled");
        }
        return filledPoints;
    }

    @Override
    public ServiceResponseEntity fill(int x, int y, char c) {
        IPoint point = shapeFactory.newPoint(x,y);
        ICanvas canvas = null;
        try {
            canvas = canvasRepository.getCanvas();
        }catch (NotInitializedException e){
            e.printStackTrace();
            return ServiceResponseEntity.FAILED_RESPONSE("canvas NotInitializedException occurred");
        }

        if(canvas==null){
            return ServiceResponseEntity.FAILED_RESPONSE("Coudn't fetch canvas");
        }

        if(shapeValidationService.isValid(point) && shapeValidationService.isInCanvas(point,canvas)){
            if(canvas.getGrid()[point.getY()-1][point.getX()-1]!= canvasRepository.DEFAULT_CHAR){
                canvas =  canvasRepository.addPoints(this.filledPoints(canvas,point),c);
                try {
                    boolean result = canvasRepository.saveCanvas(canvas);
                    return ServiceResponseStepBuilder.builder(result)
                            .success("Canvas saved Successfully")
                            .error("Error occurred while saving canvas.")
                            .build();
                }catch (FailedToSaveException e){
                    e.printStackTrace();
                    log.error("Failed to save canvas");
                    return ServiceResponseEntity
                            .FAILED_RESPONSE("FailedToSaveException in saving canvas");
                }
            }else {
                log.error("Start fill point is marked");
                return ServiceResponseEntity.FAILED_RESPONSE("Point is marked");
            }
        }
        log.error("Invalid Fill start point");
        return ServiceResponseEntity.FAILED_RESPONSE("Point is invalid");
    }

    private boolean[][] makeIsVisited(ICanvas canvas) {
        boolean[][] isVisited = new boolean[canvas.getHeight()][canvas.getWidth()];
        for(int i=0;i<canvas.getHeight();i++)
            for (int j=0;j<canvas.getWidth();j++){
                if(canvas.getGrid()[i][j] != '\u0000'){
                    isVisited[i][j] = true;
                }
            }
            return isVisited;
    }

    private boolean isValidPoint(ICanvas canvas,IPoint point,boolean[][] isVisited){

        return (point.getX() > 0 && point.getX() <= canvas.getWidth())
                && (point.getY() > 0 && point.getY() <= canvas.getHeight())
                && !isVisited[point.getY() - 1][point.getX() - 1];
    }

    private List<IPoint> getChildren(IPoint point){
        List<IPoint> points = new ArrayList<>();

        int[] c = {1,-1,0};

        for(int i=0;i<c.length;i++){
            for (int j=0;j<c.length;j++){
                IPoint p = shapeFactory.newPoint(point.getX()+c[i],point.getY()+c[j]);
                if(!p.equals(point)){
                    points.add(p);
                }
            }
        }

        return points;
    }
}
