package com.cs.test.paint.service.validation;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.service.validation.rules.CanvasValidationRules;
import org.springframework.stereotype.Service;

@Service
public class CanvasValidationService implements ICanvasValidationService {

    @Override
    public boolean isValid(ICanvas canvas) {
        return CanvasValidationRules.validationRules().allMatch(p-> p.test(canvas));
    }
}
