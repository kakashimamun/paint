package com.cs.test.paint.service;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.IShape;
import com.cs.test.paint.service.entity.ServiceResponseEntity;

public interface ICanvasService {

    /**
     * Create a {@link ICanvas} and save to Repository
     * @param width
     * @param height
     * @return
     */
    ServiceResponseEntity createCanvas(int width, int height);

    /**
     * get {@link ICanvas} from repository
     * @return
     */
    ICanvas getCanvas();

    /**
     * Adds a shape to the {@link ICanvas} and save it to repository
     * @param shape
     * @return
     */
    ServiceResponseEntity addShape(IShape shape);

    /**
     * get {@link ICanvas} pixel data
     * @return
     */
    char[][] getCanvasData();

}
