package com.cs.test.paint.service.entity;

/**
 * an Step builder to build {@link ServiceResponseEntity}
 */
public class ServiceResponseStepBuilder {

    public static SuccessStep builder(boolean result) {
        return new Builder().add(result);
    }

    public interface SuccessStep {
        ErrorStep success(String msg);
    }
    public interface ErrorStep {

        Build error(String msg);
    }

    public interface Build {
        ServiceResponseEntity build();
    }

    public static class Builder implements SuccessStep,ErrorStep, Build {
        boolean result;
        private String success;
        private String error;

        private SuccessStep add(boolean result) {
            this.result = result;
            return this;
        }

        @Override
        public ErrorStep success(String msg) {
            this.success = msg;
            return this;
        }

        @Override
        public Build error(String msg) {
            this.error = msg;
            return this;
        }
        @Override
        public ServiceResponseEntity build() {
            if(result)
                return ServiceResponseEntity.builder().success(result).msg(success).build();
            else
                return ServiceResponseEntity.builder().success(result).msg(error).build();
        }
    }
}


