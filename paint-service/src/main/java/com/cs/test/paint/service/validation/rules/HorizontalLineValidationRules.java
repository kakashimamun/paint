package com.cs.test.paint.service.validation.rules;

import com.cs.test.paint.model.interfaces.ILine;
import com.cs.test.paint.model.interfaces.IPoint;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

/**
 * Validation rules for {@link com.cs.test.paint.model.HorizontalLine}
 */
public class HorizontalLineValidationRules {

    public static Predicate<ILine> validationRules(){
        return l->l.getA().getY()==l.getB().getY();
    }

    public static BiPredicate<IPoint,IPoint> isHorizontalLine(){
        return (a,b)->a.getY()==b.getY();
    }

    public static String explanation(){
        return "For horizontal lines between (x1,y1) & (x2,y2):  y1==y2";
    }
}
