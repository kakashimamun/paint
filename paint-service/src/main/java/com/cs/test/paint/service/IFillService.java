package com.cs.test.paint.service;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.IPoint;
import com.cs.test.paint.service.entity.ServiceResponseEntity;

import java.util.List;

public interface IFillService {
    /**
     * Find {@link IPoint}s to fill starting from givem {@link IPoint} instance in the given {@link ICanvas}
     * @param canvas
     * @param point
     * @return
     */
    List<IPoint> filledPoints(ICanvas canvas, IPoint point);

    /**
     * fill empty pixels with {@param c}
     * @param x
     * @param y
     * @param c
     * @return
     */
    ServiceResponseEntity fill(int x, int y, char c);
}
