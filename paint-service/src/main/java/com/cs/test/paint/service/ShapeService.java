package com.cs.test.paint.service;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.IPoint;
import com.cs.test.paint.model.interfaces.IShape;
import com.cs.test.paint.model.interfaces.IShapeFactory;
import com.cs.test.paint.service.entity.ServiceResponseEntity;
import com.cs.test.paint.service.validation.rules.VerticalLineValidationRules;
import com.cs.test.paint.service.validation.IShapeValidationService;
import com.cs.test.paint.service.validation.rules.HorizontalLineValidationRules;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j
public class ShapeService implements IShapeService {

    @Autowired
    IShapeValidationService validationService;

    @Autowired
    IShapeFactory shapeFactory;

    @Autowired
    ICanvasService canvasService;

    @Override
    public ServiceResponseEntity addLine(int x1, int y1, int x2, int y2){

        ICanvas canvas = canvasService.getCanvas();

        IShape a = shapeFactory.newPoint(x1,y1);
        IShape b = shapeFactory.newPoint(x2,y2);

        IShape line = null;
        if(VerticalLineValidationRules.isVerticalLine().test((IPoint)a,(IPoint)b)){
            line = shapeFactory.newVerticalLine((IPoint)a,(IPoint)b);
        }else if(HorizontalLineValidationRules.isHorizontalLine().test((IPoint)a,(IPoint)b)){
            line = shapeFactory.newHorizontalLine((IPoint)a,(IPoint)b);
        }

        if(line != null){
            if(validationService.isValid(line) && validationService.isInCanvas(line,canvas)){
                return canvasService.addShape(line);
            }else
                return ServiceResponseEntity.FAILED_RESPONSE("validation failed.Invalid line.");
        }
        return ServiceResponseEntity.FAILED_RESPONSE("Error in adding line. Unsupported shape");
    }

    @Override
    public ServiceResponseEntity addRectangle(int x1, int y1, int x2, int y2){

        ICanvas canvas = canvasService.getCanvas();
        IShape a = shapeFactory.newPoint(x1,y1);
        IShape b = shapeFactory.newPoint(x2,y2);

        IShape rectangle = null;

        rectangle = shapeFactory.newRectangle((IPoint)a,(IPoint)b);

        if(rectangle != null){
            if(validationService.isValid(rectangle) && validationService.isInCanvas(rectangle,canvas)){
                return canvasService.addShape(rectangle);
            }
        }
        return ServiceResponseEntity.FAILED_RESPONSE("Error in adding rectangle.");
    }
}
