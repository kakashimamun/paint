package com.cs.test.paint.service.validation;

import com.cs.test.paint.model.interfaces.*;
import com.cs.test.paint.service.validation.rules.LineValidationRules;
import com.cs.test.paint.service.validation.rules.PointValidationRules;
import org.springframework.stereotype.Service;

@Service
public class ShapeValidationService implements IShapeValidationService {
    @Override
    public boolean isValid(IShape shape){

        if(shape instanceof IPoint){
            return PointValidationRules.validationRules().allMatch(p-> p.test((IPoint)shape));
        }else if(shape instanceof ILine){
            return LineValidationRules.validationRules().allMatch(p-> p.test((ILine)shape));
        }else if(shape instanceof IRectangle){
            IRectangle rectangle  = (IRectangle) shape;

            return isValid(rectangle.getVL())
                    && isValid(rectangle.getVR())
                    && isValid(rectangle.getHL())
                    && isValid(rectangle.getHU())
                    && !rectangle.getVL().equals(rectangle.getVR())
                    && !rectangle.getHL().equals(rectangle.getHU());
        }
        return false;
    }

    @Override
    public boolean isInCanvas(IShape shape, ICanvas canvas) {
        return shape.getPoints()
                .stream()
                .noneMatch(s->s.getX()>canvas.getWidth() || s.getY()>canvas.getHeight()
                        || s.getX()<=0 || s.getY()<=0);
    }
}
