package com.cs.test.paint.service.validation;

import com.cs.test.paint.model.interfaces.ICanvas;

public interface ICanvasValidationService {

    /**
     * Validate {@link ICanvas} instance
     * @param canvas
     * @return
     */
    boolean isValid(ICanvas canvas);
}
