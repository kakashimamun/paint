package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.ILine;
import com.cs.test.paint.model.interfaces.IPoint;
import com.cs.test.paint.model.interfaces.IRectangle;
import com.cs.test.paint.model.interfaces.IShapeFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ShapeFactoryTest {

    IShapeFactory factory;

    @Before
    public void createFactory(){
        this.factory = new ShapeFactory();
    }

    @Test
    public void newPoint() throws Exception {
        IPoint p = factory.newPoint(15, 15);
        Assert.assertTrue(new Point(15,15).equals(p));
    }

    @Test
    public void newLine() throws Exception {
        ILine l = factory.newLine(new Point(10,10),new Point(12,12));

        Assert.assertNull(l);
    }

    @Test
    public void newHorizontalLine() throws Exception {
        ILine l = factory.newVerticalLine(new Point(2,12),new Point(12,12));
        Assert.assertTrue(l.equals(new VerticalLine(new Point(2,12),new Point(12, 12))));
    }

    @Test
    public void newVerticalLine() throws Exception {
        ILine l = factory.newHorizontalLine(new Point(2,2),new Point(2,12));
        Assert.assertTrue(l.equals(new HorizontalLine(new Point(2,2),new Point(2, 12))));
    }

    @Test
    public void newRectangle() throws Exception {
        IPoint uL = new Point(2,2);
        IPoint lR = new Point(9,9);

        IPoint lL = new Point(uL.getX(),lR.getY());
        IPoint uR = new Point(lR.getX(),uL.getY());

        ILine a = new HorizontalLine(uL,lL);
        ILine b = new VerticalLine(uL,uR);
        ILine c = new HorizontalLine(uR,lR);
        ILine d = new VerticalLine(lL,lR);

        IRectangle rectangle = new Rectangle(a,b,c,d);

        IRectangle r = factory.newRectangle(new Point(2,2),new Point(9,9));

        Assert.assertTrue(r.equals(rectangle));
    }

}