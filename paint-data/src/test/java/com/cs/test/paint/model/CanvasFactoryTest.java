package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.ICanvasFactory;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CanvasFactoryTest {
    @Test
    public void createCanvas() throws Exception {
        ICanvasFactory iCanvasFactory = new CanvasFactory();

        ICanvas canvas = iCanvasFactory.createCanvas(100,10);

        Assert.assertEquals(canvas.getHeight(),10);
        Assert.assertEquals(canvas.getWidth(),100);
        Assert.assertEquals(canvas.getGrid().length,10);
        Assert.assertEquals(canvas.getGrid()[0].length,100);

        org.springframework.util.Assert.notNull(canvas.getGrid(),"Grid is not initialized");
    }

}