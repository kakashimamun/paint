package com.cs.test.paint.repositories;

import com.cs.test.paint.exception.NotInitializedException;
import com.cs.test.paint.model.Canvas;
import com.cs.test.paint.model.Point;
import com.cs.test.paint.model.interfaces.ICanvas;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class CanvasRepositoryTest {

    ICanvasRepository repository;

    @Before
    public void createRepo(){
        this.repository = new CanvasRepository();
    }

    @Test
    public void createCanvas() throws Exception {
        ICanvas canvas = new Canvas(10,10);
        Assert.assertTrue(repository.createCanvas(canvas));
    }


    @Test
    public void addPoints() throws Exception {
        ICanvas canvas = new Canvas(10,10);

        repository.createCanvas(canvas);

        repository.addPoints(Arrays.asList(new Point(2,2)));

        Assert.assertTrue(repository.getCanvas().getGrid()[1][1] == repository.DEFAULT_CHAR);
    }

    @Test
    public void addPointsWithChar() throws Exception {
        ICanvas canvas = new Canvas(10,10);

        repository.createCanvas(canvas);

        repository.addPoints(Arrays.asList(new Point(5,5)),'o');

        Assert.assertTrue(repository.getCanvas().getGrid()[4][4] == 'o');
        Assert.assertTrue(repository.getCanvas().getGrid()[6][6] == '\u0000');
        Assert.assertFalse(repository.getCanvas().getGrid()[4][4] == '\u0000');
    }

    @Test(expected = NotInitializedException.class)
    public void getCanvasNotInitialized() throws Exception {

        ICanvas canvas = repository.getCanvas();
    }

}