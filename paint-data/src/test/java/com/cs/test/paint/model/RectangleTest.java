package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.ILine;
import com.cs.test.paint.model.interfaces.IPoint;
import com.cs.test.paint.model.interfaces.IRectangle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RectangleTest {

    IRectangle rectangle;

    @Before
    public void createRectangle(){
        IPoint uL = new Point(2,2);
        IPoint lR = new Point(9,9);

        IPoint lL = new Point(uL.getX(),lR.getY());
        IPoint uR = new Point(lR.getX(),uL.getY());

        ILine a = new HorizontalLine(uL,lL);
        ILine b = new VerticalLine(uL,uR);
        ILine c = new HorizontalLine(uR,lR);
        ILine d = new VerticalLine(lL,lR);

        rectangle = new Rectangle(a,b,c,d);

    }

    @Test
    public void getVL() throws Exception {

        Assert.assertEquals(rectangle.getVL().getA().getX(),2);
        Assert.assertEquals(rectangle.getVL().getA().getY(),2);
        Assert.assertEquals(rectangle.getVL().getB().getX(),2);
        Assert.assertEquals(rectangle.getVL().getB().getY(),9);

    }

    @Test
    public void getVR() throws Exception {
        Assert.assertEquals(rectangle.getVR().getA().getX(),9);
        Assert.assertEquals(rectangle.getVR().getA().getY(),2);
        Assert.assertEquals(rectangle.getVR().getB().getX(),9);
        Assert.assertEquals(rectangle.getVR().getB().getY(),9);
    }

    @Test
    public void getHU() throws Exception {
        Assert.assertEquals(rectangle.getHU().getA().getX(),2);
        Assert.assertEquals(rectangle.getHU().getA().getY(),2);
        Assert.assertEquals(rectangle.getHU().getB().getX(),9);
        Assert.assertEquals(rectangle.getHU().getB().getY(),2);
    }

    @Test
    public void getHL() throws Exception {
        Assert.assertEquals(rectangle.getHL().getA().getX(),2);
        Assert.assertEquals(rectangle.getHL().getA().getY(),9);
        Assert.assertEquals(rectangle.getHL().getB().getX(),9);
        Assert.assertEquals(rectangle.getHL().getB().getY(),9);
    }

    @Test
    public void equals() throws Exception {

        IPoint uL = new Point(2,2);
        IPoint lR = new Point(9,9);

        IPoint lL = new Point(uL.getX(),lR.getY());
        IPoint uR = new Point(lR.getX(),uL.getY());

        ILine a = new HorizontalLine(uL,lL);
        ILine b = new VerticalLine(uL,uR);
        ILine c = new HorizontalLine(uR,lR);
        ILine d = new VerticalLine(lR,lL);

        IRectangle r = new Rectangle(a,b,c,d);

        Assert.assertTrue(r.equals(this.rectangle));
    }
}