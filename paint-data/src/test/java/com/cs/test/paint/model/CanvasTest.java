package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.IPoint;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CanvasTest {

    @Test
    public void cloneTest() throws Exception {
        int height = 5;
        int width = 5;
        ICanvas canvas = new Canvas(width,height);
        ICanvas cloned = canvas.clone();

        Assert.assertNotEquals(canvas,cloned);
        Assert.assertEquals(canvas.getWidth(),cloned.getWidth());
        Assert.assertEquals(canvas.getHeight(),cloned.getHeight());
        for(int i=0;i<width;i++)
            for (int j=0;j<height;j++)
            Assert.assertEquals(canvas.getGrid()[i][j],cloned.getGrid()[i][j]);
    }

    @Test
    public void addPoint() throws Exception {
        int height = 5;
        int width = 5;
        ICanvas canvas = new Canvas(width,height);

        int x = 4;
        int y = 4;

        IPoint p = new Point(x,y);
        canvas.addPoint(p,'s');

        Assert.assertEquals(canvas.getGrid()[x-1][y-1],'s');
        Assert.assertNotEquals(canvas.getGrid()[x][y],'s');
    }

}