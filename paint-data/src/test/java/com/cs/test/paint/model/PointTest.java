package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.IPoint;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PointTest {
    @Test
    public void equals() throws Exception {

        IPoint p1 = new Point(5,3);
        IPoint p2 = new Point(5,3);
        IPoint p3 = new Point(3,3);

        Assert.assertTrue(p1.equals(p2));
        Assert.assertTrue(p2.equals(p1));
        Assert.assertFalse(p3.equals(p2));
        Assert.assertFalse(p1.equals(p3));

    }

}