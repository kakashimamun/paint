package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.ILine;
import com.cs.test.paint.model.interfaces.IPoint;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class VerticalLineTest {
    @Test
    public void getPoints() throws Exception {
        List<IPoint> points = Arrays.asList(
                new Point(2,2),
                new Point(2,3),
                new Point(2,4),
                new Point(2,5),
                new Point(2,6),
                new Point(2,7)
                );

        ILine line = new VerticalLine(new Point(2,2),new Point(2,7));

        org.springframework.util.Assert.notEmpty(line.getPoints(),"Line doesn't have points");

        List<IPoint> linePoints = line.getPoints();

        for(int i=0;i<linePoints.size();i++){
            Assert.assertEquals(points.get(i).getX(),linePoints.get(i).getX());
            Assert.assertEquals(points.get(i).getY(),linePoints.get(i).getY());
        }

        Assert.assertEquals(points.size(),linePoints.size());
    }

    @Test
    public void getA() throws Exception {
        ILine line = new VerticalLine(new Point(2,2),new Point(7,2));
        IPoint p = line.getA();

        Assert.assertEquals(p.getX(),2);
        Assert.assertEquals(p.getY(),2);
    }

    @Test
    public void getB() throws Exception {

        ILine line = new VerticalLine(new Point(2,2),new Point(7,2));
        IPoint p = line.getB();

        Assert.assertEquals(p.getX(),7);
        Assert.assertEquals(p.getY(),2);
    }

    @Test
    public void equals() throws Exception {
        ILine line1 = new VerticalLine(new Point(2,2),new Point(7,2));
        ILine line2 = new VerticalLine(new Point(7,2),new Point(2,2));
        ILine line3 = new VerticalLine(new Point(7,3),new Point(2,3));

        Assert.assertTrue(line1.equals(line2));
        Assert.assertTrue(line2.equals(line1));
        Assert.assertTrue(line1.equals(line1));

        Assert.assertFalse(line1.equals(line3));
    }

}