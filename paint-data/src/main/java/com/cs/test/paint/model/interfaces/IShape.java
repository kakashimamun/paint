package com.cs.test.paint.model.interfaces;

import java.util.List;

public interface IShape {
    /**
     * Get all points of a {@link IShape}
     * @return
     */
    public List<IPoint> getPoints();
}
