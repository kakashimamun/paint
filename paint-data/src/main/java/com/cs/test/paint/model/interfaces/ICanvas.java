package com.cs.test.paint.model.interfaces;



public interface ICanvas extends Cloneable {
    ICanvas clone() throws CloneNotSupportedException;
    int getWidth();
    int getHeight();
    char[][] getGrid();

    /**
     * Add a {@link IPoint} in canvas grid with c char
     * @param point
     * @param c
     */
    void addPoint(IPoint point,char c);
}
