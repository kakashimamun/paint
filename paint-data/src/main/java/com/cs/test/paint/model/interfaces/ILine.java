package com.cs.test.paint.model.interfaces;

public interface ILine extends IShape {

    IPoint getA();
    IPoint getB();

    boolean equals(ILine line);
}
