package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.*;
import org.springframework.stereotype.Component;

@Component
public class ShapeFactory implements IShapeFactory {

    @Override
    public IPoint newPoint(int x, int y){
        return new Point(x,y);
    }

    @Override
    public ILine newLine(IPoint a, IPoint b){
        return null;
    }

    @Override
    public ILine newVerticalLine(IPoint a, IPoint b){
        return new VerticalLine(a,b);
    }

    @Override
    public ILine newHorizontalLine(IPoint a, IPoint b){
        return new HorizontalLine(a,b);
    }

    @Override
    public IRectangle newRectangle(IPoint uL, IPoint lR){

        IPoint lL = newPoint(uL.getX(),lR.getY());
        IPoint uR = newPoint(lR.getX(),uL.getY());
        ILine a = newVerticalLine(uL,lL);
        ILine b = newHorizontalLine(uL,uR);
        ILine c = newVerticalLine(uR,lR);
        ILine d = newHorizontalLine(lL,lR);

        return new Rectangle(a,b,c,d);
    }

}
