package com.cs.test.paint.model.interfaces;

public interface IShapeFactory {

    /**
     * create a new {@link IPoint}
     * @param x
     * @param y
     * @return
     */
    IPoint newPoint(int x, int y);

    /**
     * create new {@link ILine}
     * @param a
     * @param b
     * @return
     */
    ILine newLine(IPoint a, IPoint b);

    /**
     * vertical line
     * @param a
     * @param b
     * @return
     */
    ILine newVerticalLine(IPoint a, IPoint b);

    /**
     * new horizontal line
     * @param a
     * @param b
     * @return
     */
    ILine newHorizontalLine(IPoint a, IPoint b);

    /**
     * new Rectangle
     * @param uL
     * @param lR
     * @return
     */
    IRectangle newRectangle(IPoint uL, IPoint lR);
}
