package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.ILine;
import com.cs.test.paint.model.interfaces.IPoint;
import com.cs.test.paint.model.interfaces.IRectangle;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class Rectangle implements IRectangle {

    /**
     * vL = vertical left
     * hU = horizontal Upper
     * vR = vertical right
     * hL = horizontal lower
     */
    ILine vL,hU,vR,hL;

    @Override
    public ILine getVL() {
        return vL;
    }

    @Override
    public ILine getVR() {
        return vR;
    }

    @Override
    public ILine getHU() {
        return hU;
    }

    @Override
    public ILine getHL() {
        return hL;
    }

    @Override
    public boolean equals(IRectangle rectangle) {
        return vL.equals(rectangle.getVL()) && vR.equals(rectangle.getVR());
    }

    @Override
    public List<IPoint> getPoints() {
        List<IPoint> points = new ArrayList<>();
        points.addAll(vL.getPoints());
        points.addAll(vR.getPoints());
        points.addAll(hU.getPoints());
        points.addAll(hL.getPoints());
        return points;
    }
}
