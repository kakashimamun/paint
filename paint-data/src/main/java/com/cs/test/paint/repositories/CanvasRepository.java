package com.cs.test.paint.repositories;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.exception.NotInitializedException;
import com.cs.test.paint.model.interfaces.IPoint;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Log4j
public class CanvasRepository implements ICanvasRepository {

    /**
     * Persistent Data
     */
    ICanvas canvas;

    @Override
    public boolean createCanvas(ICanvas canvas){
        this.canvas  = canvas;
        log.info("Canvas created successfully");
        return true;
    }

    @Override
    public boolean saveCanvas(ICanvas canvas){
        try {
            this.canvas = canvas.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            log.error("Error in saving canvas");
            return false;
        }
        log.info("Canvas saved successfully");
        return true;
    }

    @Override
    public ICanvas getCanvas() {
        if(canvas!=null){
            try {
                return canvas.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                log.error("Canvas can't be cloned while getting");
                return null;
            }
        }else {
            log.error("Canvas not initializer error");
            throw new NotInitializedException();

        }
    }

    @Override
    public ICanvas addPoints(List<IPoint> points) {
        return addPoints(points,DEFAULT_CHAR);
    }

    @Override
    public ICanvas addPoints(List<IPoint> points,char c) {

        for(IPoint point:points){
            try {
                this.canvas.addPoint(point,c);
            }catch (ArrayIndexOutOfBoundsException e){
                e.printStackTrace();
                log.error("Error happened while adding point to canvas.ArrayIndexOutOfBoundsException for {}.{}", e);
            }
        }
        return this.canvas;
    }
}
