package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.ICanvasFactory;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Component;
import com.cs.test.paint.exception.FailedToCreateException;

@Component
public class CanvasFactory implements ICanvasFactory{

    @Override
    public ICanvas createCanvas(int width, int height) throws FailedToCreateException {
        return new Canvas(width,height);
    }
}
