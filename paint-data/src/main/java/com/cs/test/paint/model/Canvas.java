package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.model.interfaces.IPoint;
import com.cs.test.paint.model.interfaces.IShape;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.beans.Transient;
import java.util.List;



public class Canvas implements ICanvas {

    int width;
    int height;
    char[][] grid;

    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;
        this.grid = new char[this.height][this.width];
    }

    @Override
    public Canvas clone() throws CloneNotSupportedException {
        Canvas clonedObj = (Canvas) super.clone();
        clonedObj.width = this.width;
        clonedObj.height = this.height;
        for(int i=0;i<clonedObj.height;i++){
            System.arraycopy(grid[i], 0, clonedObj.grid[i], 0, clonedObj.width);
        }
        return clonedObj;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public char[][] getGrid() {
        return this.grid;
    }

    @Override
    public void addPoint(IPoint point, char c) {
        this.grid[point.getY()-1][point.getX()-1] = c;
    }
}
