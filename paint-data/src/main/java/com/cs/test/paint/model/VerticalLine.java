package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.IPoint;
import lombok.Builder;

import java.util.ArrayList;
import java.util.List;

public class VerticalLine extends AbstractLine {
    public VerticalLine(IPoint a, IPoint b) {
        super(a, b);
    }

    @Override
    public List<IPoint> getPoints() {
        List<IPoint> points = new ArrayList<>();

        int min = Math.min(a.getY(),b.getY());
        int max = Math.max(a.getY(),b.getY());

        for(int i=min;i<=max;i++){
            points.add(new Point(a.getX(),i));
        }
        return points;
    }
}
