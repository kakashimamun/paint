package com.cs.test.paint.model.interfaces;

public interface IRectangle extends IShape {

    /**
     * Left vertical line
     * @return
     */
    ILine getVL();

    /**
     * Right vertical line
     * @return
     */
    ILine getVR();

    /**
     * Upper horizontal line
     * @return
     */
    ILine getHU();

    /**
     * Lower horizontal line
     * @return
     */
    ILine getHL();

    boolean equals(IRectangle rectangle);
}
