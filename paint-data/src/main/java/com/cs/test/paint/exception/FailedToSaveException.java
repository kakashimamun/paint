package com.cs.test.paint.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FailedToSaveException extends RuntimeException {
    FailedToSaveException(String msg){
        super(msg);
    }
}
