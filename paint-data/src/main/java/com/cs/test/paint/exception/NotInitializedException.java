package com.cs.test.paint.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class NotInitializedException extends RuntimeException {
    NotInitializedException(String msg){
        super(msg);
    }
}
