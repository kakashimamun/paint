package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.IPoint;
import lombok.Builder;

import java.util.ArrayList;
import java.util.List;

public class HorizontalLine extends AbstractLine {
    public HorizontalLine(IPoint a, IPoint b) {
        super(a, b);
    }

    @Override
    public List<IPoint> getPoints() {
        List<IPoint> points = new ArrayList<>();
        int min = Math.min(a.getX(),b.getX());
        int max = Math.max(a.getX(),b.getX());

        for(int i=min;i<=max;i++){
            points.add(new Point(i,a.getY()));
        }
        return points;
    }
}
