package com.cs.test.paint.model.interfaces;

import com.cs.test.paint.exception.FailedToCreateException;

public interface ICanvasFactory {
    /**
     * Create an instance of {@link ICanvas} and return it
     * @param width
     * @param height
     * @return
     * @throws FailedToCreateException
     */
    ICanvas createCanvas(int width, int height) throws FailedToCreateException;
}
