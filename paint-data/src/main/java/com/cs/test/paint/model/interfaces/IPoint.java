package com.cs.test.paint.model.interfaces;


public interface IPoint extends IShape{
    public int getX();
    public int getY();

    boolean equals(IPoint point);
}
