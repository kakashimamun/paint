package com.cs.test.paint.repositories;

import com.cs.test.paint.model.interfaces.ICanvas;
import com.cs.test.paint.exception.FailedToCreateException;
import com.cs.test.paint.exception.NotInitializedException;
import com.cs.test.paint.exception.FailedToSaveException;
import com.cs.test.paint.model.interfaces.IPoint;

import java.util.List;

public interface ICanvasRepository{

    char DEFAULT_CHAR = 'x';

    /**
     * Add list of {@link IPoint} in canvas and return updated canvass
     * @param points
     * @return
     */
    ICanvas addPoints(List<IPoint> points);

    /**
     * Add list of {@link IPoint} in canvas with char c and return updated canvass
     * @param points
     * @param c
     * @return
     */
    ICanvas addPoints(List<IPoint> points, char c);

    /**
     * create an instance of {@link ICanvas} i
     * @param canvas
     * @return success or fail
     * @throws FailedToCreateException
     */
    boolean createCanvas(ICanvas canvas) throws FailedToCreateException;

    /**
     * save an {@link ICanvas}
     * @param canvas
     * @return success or fail
     * @throws FailedToSaveException
     */
    boolean saveCanvas(ICanvas canvas) throws FailedToSaveException;

    /**
     * get {@link ICanvas}
     * @return
     * @throws NotInitializedException
     */
    ICanvas getCanvas() throws NotInitializedException;

}
