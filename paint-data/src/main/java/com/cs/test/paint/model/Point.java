package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.IPoint;
import com.cs.test.paint.model.interfaces.IShape;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public class Point implements IPoint {
    private int x;
    private int y;


    @Override
    public boolean equals(IPoint point) {
        return (x == point.getX() && y == point.getY());
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public List<IPoint> getPoints() {
        return Collections.singletonList(this);
    }
}
