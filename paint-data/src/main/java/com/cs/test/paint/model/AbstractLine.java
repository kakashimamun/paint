package com.cs.test.paint.model;

import com.cs.test.paint.model.interfaces.ILine;
import com.cs.test.paint.model.interfaces.IPoint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

public abstract class AbstractLine implements ILine {
    IPoint a;
    IPoint b;

    public AbstractLine(IPoint a, IPoint b){
        this.a = a;
        this.b = b;
    }

    @Override
    public IPoint getA() {
        return a;
    }

    @Override
    public IPoint getB() {
        return b;
    }

    @Override
    public boolean equals(ILine line){
       return (this.a.equals(line.getA())&&this.b.equals(line.getB()))
               || (this.b.equals(line.getA())&&this.a.equals(line.getB()));
    }
}
