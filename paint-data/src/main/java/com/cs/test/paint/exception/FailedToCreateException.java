package com.cs.test.paint.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FailedToCreateException extends RuntimeException {
    FailedToCreateException(String msg){
        super(msg);
    }
}
