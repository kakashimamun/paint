# README #

### What is this repository for? ###
This is a simple OOP based console drawing program. Fuctionalities for the project described in `problem-statement` file
###### Assumptions made ######
* Shapes can be drawn on top of filled points unless it's filled by `x`
* Fill command `B x y c` will change the starting point to `c` unless the point is the character`x`
* Fill command `B x y c` will only fill pixel having `'\u0000'` value.

### How do I get set up? ###

* Dependencies
    * Maven 3+
    * JRE 8+
* How to run tests
     * run `mvn test` in root folder
* How to run
    * run `mvn clean install` in root folder
    * go inside `paint-shell` folder
    *  run `mvn spring-boot:run`
    *  follow the command line UI
    
